################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/008_spi_cmd_handling.c \
../Src/syscalls.c \
../Src/sysmem.c 

OBJS += \
./Src/008_spi_cmd_handling.o \
./Src/syscalls.o \
./Src/sysmem.o 

C_DEPS += \
./Src/008_spi_cmd_handling.d \
./Src/syscalls.d \
./Src/sysmem.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o Src/%.su: ../Src/%.c Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DSTM32 -DSTM32F4 -DSTM32F411RETx -c -I"C:/Users/Anuj/Documents/git/stm32f411re-drivers-project/Drivers/Inc" -I"C:/Users/Anuj/Documents/git/stm32f411re-drivers-project/Drivers/Src" -I"C:/Users/Anuj/Documents/git/stm32f411re-drivers-project/Inc" -I"C:/Users/Anuj/Documents/git/stm32f411re-drivers-project/Src" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Src

clean-Src:
	-$(RM) ./Src/008_spi_cmd_handling.d ./Src/008_spi_cmd_handling.o ./Src/008_spi_cmd_handling.su ./Src/syscalls.d ./Src/syscalls.o ./Src/syscalls.su ./Src/sysmem.d ./Src/sysmem.o ./Src/sysmem.su

.PHONY: clean-Src

