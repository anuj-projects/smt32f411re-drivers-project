/*
 * stm32f411re_spi_driver.h
 *
 *  Created on: Mar 20, 2022
 *      Author: Anuj
 */

#ifndef INC_STM32F411RE_SPI_DRIVER_H_
#define INC_STM32F411RE_SPI_DRIVER_H_

#include "stm32f411re.h"

/*
 *  Configuration structure for SPIx peripheral
 */

typedef struct
{
	uint8_t SPI_DeviceMode;
	uint8_t SPI_BusConfig;
	uint8_t SPI_SclkSpeed;
	uint8_t SPI_DFF;
	uint8_t SPI_CPOL;
	uint8_t SPI_CPHA;
	uint8_t SPI_SSM;
}SPI_Config_t;

/*
 *  Handle structure for SPIx peripheral
 */

typedef struct
{
	SPI_RegDef_t						*pSPIx;		/* This holds the base address of SPIx( where x = 1,2,3,4,5) peripheral*/
	SPI_Config_t						SPIConfig;
}SPI_Handle_t;

/*
 *  @SPI_DeviceMode
 */

#define SPI_DEVICE_MODE_MASTER					1
#define SPI_DEVICE_MODE_SLAVE					0

/*
 *  @SPI_BusConfig
 */

#define SPI_BUS_CONFIG_FD						1	// Full Duplex
#define SPI_BUS_CONFIG_HD						2 	// Half Duplex
#define SPI_BUS_CONFIG_SIMPLEX_RXONLY			3	// Simplex Receive only

/*
 *  @SPI_SclkSpeed
 */

#define SPI_SCLK_SPEED_DIV2						0	// 000 for dividing fPCLK by 2
#define SPI_SCLK_SPEED_DIV4						1	// 001 for dividing fPCLK by 4
#define SPI_SCLK_SPEED_DIV8						2	// 010 for dividing fPCLK by 8
#define SPI_SCLK_SPEED_DIV16					3	// 011 for dividing fPCLK by 16
#define SPI_SCLK_SPEED_DIV32					4	// 100 for dividing fPCLK by 32
#define SPI_SCLK_SPEED_DIV64					5	// 101 for dividing fPCLK by 64
#define SPI_SCLK_SPEED_DIV128					6	// 110 for dividing fPCLK by 128
#define SPI_SCLK_SPEED_DIV256					7	// 111 for dividing fPCLK by 256

/*
 *  @SPI_DFF
 */

#define SPI_DFF_8BITS							0	// Default is 8 Bits
#define SPI_DFF_16BITS							1

/*
 *  @SPI_CPOL
 */

#define SPI_CPOL_HIGH								1	// Clock polarity
#define SPI_CPOL_LOW								0	// Clock polarity

/*
 *  @SPI_CPHA
 */

#define SPI_CPHA_HIGH								1	// Clock phase
#define SPI_CPHA_LOW								0	// Clock phase

/*
 *  @SPI_SSM, software slave management
 */

#define SPI_SSM_EN									1	// Default to software SSM 0 is disabled, Software slave management disabled
#define SPI_SSM_DI									0	// Software slave management enabled

/*
 *  SPI related status flags definitions
 */

#define SPI_RXNE_FLAG								(1 << SPI_SR_RXNE)
#define SPI_TXE_FLAG 								(1 << SPI_SR_TXE)
#define SPI_CHSIDE_FLAG								(1 << SPI_SR_CHSIDE)
#define SPI_UDR_FLAG								(1 << SPI_SR_UDR)
#define SPI_CRCERR_FLAG								(1 << SPI_SR_CRCERR)
#define SPI_MODF_FLAG								(1 << SPI_SR_MODF)
#define SPI_OVR_FLAG								(1 << SPI_SR_OVR)
#define SPI_BUSY_FLAG								(1 << SPI_SR_BSY)
#define SPI_FRE_FLAG								(1 << SPI_SR_FRE)

/********************************************************************************************************************
 * 											APIs supported by this driver
 * 							For more information about the APIs check the function definitions
 ********************************************************************************************************************/
/*
 *  Peripheral Clock setup
 */

void SPI_PeriClockControl(SPI_RegDef_t *pSPIx, uint8_t EnorDi);

/*
 *  Init and De-init
 */

void SPI_Init(SPI_Handle_t *pSPIHandle);
void SPI_DeInit(SPI_RegDef_t *pSPIx);


/*
 *  Data Send and Receive
 */

void SPI_SendData(SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t Len);
void SPI_ReceiveData(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t Len);


/*
 *  IRQ Configuration and ISR handling
 */

void SPI_IRQInterruptConfig(uint8_t IRQNumber, uint8_t EnorDi);		// ASS uint32_t instead of uint8_t
void SPI_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQPriority);
void SPI_IRQHandling(SPI_Handle_t *pHandle);

/*
 *  Other Peripheral Control APIs
 */

void SPI_PeripheralControl(SPI_RegDef_t *pSPIx, uint8_t EnOrDi);
void SPI_SSIConfig(SPI_RegDef_t *pSPIx, uint8_t EnOrDi);
void SPI_SSOEConfig(SPI_RegDef_t *pSPIx, uint8_t EnOrDi);
uint8_t SPI_GetFlagStatus(SPI_RegDef_t *pSPIx, uint32_t FlagName);

#endif /* INC_STM32F411RE_SPI_DRIVER_H_ */
