/*
 * stm32f411re_spi_driver.c
 *
 *  Created on: Mar 26, 2022
 *      Author: Anuj
 */


#include "stm32f411re.h"

/*
 *  Peripheral Clock Setup
 */

/****************************************************************************************************
 * @fn						- SPI_PeriClockControl
 *
 * @brief					- This function enables or disables peripheral clock for the given SPI port
 *
 * @param[in]				- Base address of the SPI peripheral
 * @param[in]				- ENABLE or DISABLE macros
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/

void SPI_PeriClockControl(SPI_RegDef_t *pSPIx, uint8_t EnorDi)
{
	if(EnorDi == ENABLE)
	{
		if(pSPIx == SPI1)
		{
			SPI1_PCLK_EN();
		}
		else if(pSPIx == SPI2)
		{
			SPI2_PCLK_EN();
		}
		else if(pSPIx == SPI3)
		{
			SPI3_PCLK_EN();
		}
		else if(pSPIx == SPI4)
		{
			SPI4_PCLK_EN();
		}
		else if(pSPIx == SPI5)
		{
			SPI5_PCLK_EN();
		}
	}
	else
	{
		if(pSPIx == SPI1)
		{
			SPI1_PCLK_DI();
		}
		else if(pSPIx == SPI2)
		{
			SPI2_PCLK_DI();
		}
		else if(pSPIx == SPI3)
		{
			SPI3_PCLK_DI();
		}
		else if(pSPIx == SPI4)
		{
			SPI4_PCLK_DI();
		}
		else if(pSPIx == SPI5)
		{
			SPI5_PCLK_DI();
		}
	}
}

/*
 *  Init and De-init
 */

/****************************************************************************************************
 * @fn						- SPI_Init
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/
void SPI_Init(SPI_Handle_t *pSPIHandle)
{

	// peripheral clock enable
	SPI_PeriClockControl(pSPIHandle->pSPIx, ENABLE);

	// First lets configure the SPI_CR1 Register
	uint32_t tempreg = 0;

	// 1. configure the device mode
	tempreg |= pSPIHandle->SPIConfig.SPI_DeviceMode << SPI_CR1_MSTR;

	// 2. configure the bus config
	if(pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_FD)
	{
		// BIDI mode should be cleared
		tempreg &= ~(1 << SPI_CR1_BIDIMODE);
	}
	else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_HD)
	{
		// BIDI mode should be set
		tempreg |= (1 << SPI_CR1_BIDIMODE);
	}
	else if (pSPIHandle->SPIConfig.SPI_BusConfig == SPI_BUS_CONFIG_SIMPLEX_RXONLY)
	{
		// BIDI mode should be cleared
		tempreg &= ~(1 << SPI_CR1_BIDIMODE);
		// RXONLY bit must be set
		tempreg |= (1 << SPI_CR1_RXONLY);
	}

	// 3. Configure the SPI serial clock speed (baud rate)
	tempreg |= pSPIHandle->SPIConfig.SPI_SclkSpeed << SPI_CR1_BR;

	// 4. Configure the DFF (data frame format)
	tempreg |= pSPIHandle->SPIConfig.SPI_DFF << SPI_CR1_DFF;

	// 5. Configure the CPOL (clock polarity)
	tempreg |= pSPIHandle->SPIConfig.SPI_CPOL << SPI_CR1_CPOL;

	// 6. Configure the CPHA (clock phase)
	tempreg |= pSPIHandle->SPIConfig.SPI_CPHA << SPI_CR1_CPHA;

	tempreg |= pSPIHandle->SPIConfig.SPI_SSM << SPI_CR1_SSM;

	pSPIHandle->pSPIx->SPI_CR1 = tempreg;
}

/****************************************************************************************************
 * @fn						- SPI_DeInit
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/
void SPI_DeInit(SPI_RegDef_t *pSPIx)
{
	if(pSPIx == SPI1)
	{
		SPI1_REG_RESET();
	}
	else if(pSPIx == SPI2)
	{
		SPI2_REG_RESET();
	}
	else if(pSPIx == SPI3)
	{
		SPI3_REG_RESET();
	}
	else if(pSPIx == SPI4)
	{
		SPI4_REG_RESET();
	}
	else if(pSPIx == SPI5)
	{
		SPI5_REG_RESET();
	}
}

uint8_t SPI_GetFlagStatus(SPI_RegDef_t *pSPIx, uint32_t FlagName)
{
	if(pSPIx->SPI_SR & FlagName)
	{
		return FLAG_SET;
	}
	return FLAG_RESET;

}

/****************************************************************************************************
 * @fn						- SPI_SendData
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- This is a blocking call
 *
 *****************************************************************************************************/
void SPI_SendData(SPI_RegDef_t *pSPIx, uint8_t *pTxBuffer, uint32_t Len)
{
	while(Len > 0)
	{
		// 1. wait until TXE is set
		while (SPI_GetFlagStatus(pSPIx, SPI_TXE_FLAG) == FLAG_RESET);

		// 2. Check the DFF bit in CR1
		if((pSPIx->SPI_CR1 & (1 << SPI_CR1_DFF)))
		{
			// 16 BIT DFF
			// 1. Load the data in to the DR
			pSPIx->SPI_DR = *((uint16_t*)pTxBuffer);
			Len --;
			Len --;
			(uint16_t*)pTxBuffer ++;
		}
		else
		{
			// 8 BIT DFF
			pSPIx->SPI_DR = *pTxBuffer;
			Len --;
			pTxBuffer ++;
		}


	}
}
/****************************************************************************************************
 * @fn						- SPI_ReceiveData
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/
void SPI_ReceiveData(SPI_RegDef_t *pSPIx, uint8_t *pRxBuffer, uint32_t Len)
{
	while(Len > 0)
		{
			// 1. wait until RXNE is set
			while (SPI_GetFlagStatus(pSPIx, SPI_RXNE_FLAG) == FLAG_RESET);

			// 2. Check the DFF bit in CR1
			if((pSPIx->SPI_CR1 & (1 << SPI_CR1_DFF)))
			{
				// 16 BIT DFF
				// 1. Load the data from DR to RxBuffer address
				*((uint16_t*)pRxBuffer) = pSPIx->SPI_DR;
				Len --;
				Len --;
				(uint16_t*)pRxBuffer ++;
			}
			else
			{
				// 8 BIT DFF
				*(pRxBuffer) = pSPIx->SPI_DR;
				Len --;
				pRxBuffer ++;
			}


		}
}

/****************************************************************************************************
 * @fn						- SPI_PeripheralControl
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/
void SPI_PeripheralControl(SPI_RegDef_t *pSPIx, uint8_t EnOrDi)
{
	if(EnOrDi == ENABLE)
	{
		pSPIx->SPI_CR1 |= (1 << SPI_CR1_SPE);
	}
	else
	{
		pSPIx->SPI_CR1 &= ~(1 << SPI_CR1_SPE);
	}
}

/****************************************************************************************************
 * @fn						- SPI_SSIConfig
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/

void SPI_SSIConfig(SPI_RegDef_t *pSPIx, uint8_t EnOrDi)
{
	if(EnOrDi == ENABLE)
	{
		pSPIx->SPI_CR1 |= (1 << SPI_CR1_SSI);
	}
	else
	{
		pSPIx->SPI_CR1 &= ~(1 << SPI_CR1_SSI);
	}
}


/****************************************************************************************************
 * @fn						- SPI_SSOEConfig
 *
 * @brief					- This function
 *
 * @param[in]				-
 * @param[in]				-
 *
 * @return					- none
 *
 * @Note					- none
 *
 *****************************************************************************************************/

void SPI_SSOEConfig(SPI_RegDef_t *pSPIx, uint8_t EnOrDi)
{
	if(EnOrDi == ENABLE)
	{
		pSPIx->SPI_CR2 |= (1 << SPI_CR2_SSOE);
	}
	else
	{
		pSPIx->SPI_CR2 &= ~(1 << SPI_CR2_SSOE);
	}
}


